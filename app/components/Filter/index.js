import React, { useCallback } from 'react';
// import { debounce } from 'lodash';
import { Parameter } from 'components/Parameter';
import { SCHEMA_MAP } from 'schema';

function asParameter(key, parameter, index) {
  const schema = SCHEMA_MAP[key].parameters[index];
  return (
    <Parameter
      parameter={parameter}
      key={parameter.key}
      filterName={key}
      schema={schema}
    />
  );
}

function Filter({ filter }) {
  const asParameterWithKey = useCallback(asParameter.bind(null, filter.key), [
    filter.key,
  ]);
  return (
    <li>
      {filter.key}
      {filter.parameters.map(asParameterWithKey)}
    </li>
  );
}

export default Filter;
