/**
 *
 * Canvas
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { useSelect } from 'containers/Provider/effects';

import { Canvas } from 'components/Html';
import { useDrawCanvasOnLoad, useRedrawCanvasWhenFiltersChange } from './hooks';
import { ImageElement } from './styles';

export const ImageReference = React.forwardRef(
  ({ imageLayer: { objectUrl }, draw }, canvasRef) => {
    const filterLayers = useSelect(['present', 'filters', objectUrl], []);
    const images = useSelect('present.images', []);
    const imageRef = React.useRef();
    const localCanvasRef = React.useRef();
    const onLoad = useDrawCanvasOnLoad({
      canvasRef,
      draw,
      images,
      imageRef,
      localCanvasRef,
    });
    useRedrawCanvasWhenFiltersChange({
      draw,
      filterLayers,
      imageRef,
      localCanvasRef,
    });
    return (
      <Canvas id={objectUrl} ref={localCanvasRef}>
        <ImageElement onLoad={onLoad} ref={imageRef} src={objectUrl} />
      </Canvas>
    );
  }
);
