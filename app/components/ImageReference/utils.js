import { SCHEMA_MAP } from 'schema';

export function resizeCanvasToOnlyLayer(images) {
  return images.length === 1;
}

function filterLayerIntoString(filterLayer) {
  return SCHEMA_MAP[filterLayer.key].transformer(
    ...filterLayer.parameters.reduce((acc, { value, unit }) => {
      acc.push(value);
      acc.push(unit);
      return acc;
    }, [])
  );
}

export function mapFilters(filterLayers) {
  return filterLayers.length > 0
    ? filterLayers.map(filterLayerIntoString).join(' ')
    : 'none';
}
