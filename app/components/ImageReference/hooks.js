import { useCallback, useEffect } from 'react';

import { resizeCanvasToOnlyLayer, mapFilters } from './utils';

export function useDrawCanvasOnLoad({
  canvasRef,
  draw,
  imageRef,
  localCanvasRef,
  images,
}) {
  return useCallback(() => {
    const { current: canvas } = localCanvasRef;
    const { current: parentCanvas } = canvasRef;
    const { current: image } = imageRef;
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;
    if (resizeCanvasToOnlyLayer(images)) {
      parentCanvas.width = canvas.width;
      parentCanvas.height = canvas.height;
    }
    const context = canvas.getContext('2d');
    context.drawImage(image, 0, 0, canvas.width, canvas.height);
    draw();
  }, [canvasRef, draw, images, imageRef, localCanvasRef]);
}

export function useRedrawCanvasWhenFiltersChange({
  draw,
  filterLayers,
  imageRef,
  localCanvasRef,
}) {
  useEffect(() => {
    const { current: canvas } = localCanvasRef;
    const { current: image } = imageRef;
    const context = canvas.getContext('2d');
    const filterContext = mapFilters(filterLayers);
    context.filter = filterContext;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(image, 0, 0, canvas.width, canvas.height);
    draw();
  }, [draw, filterLayers, imageRef, localCanvasRef]);
}
