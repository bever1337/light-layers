/**
 *
 * FileInput
 *
 */

import React, { useContext } from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useActionCreator, useSelect } from 'containers/Provider/effects';
import { UndoableContext } from 'containers/Provider/context';
import { Button } from 'components/Html';

const urlCreator = window.URL || window.webkitURL;
const { createObjectURL } = urlCreator;

const StyledSpan = styled.span`
  background: rgba(255, 105, 30, 0.8);
  border: 5px double brown;
  border-radius: 7px;
  :active {
    box-shadow: 0 5px #666;
  }
  :hover {
    background: rgba(230, 110, 75, 1);
  }
  font-family: 'Georgia ';
  display: inline-block;
  text-shadow: 1px 1px brown;
  color: white;
`;

export function FileInput() {
  const { addImageLayer } = useActionCreator();
  const imageUrl = useSelect('imageUrl', '/');
  return (
    <form onSubmit={(evt) => evt.preventDefault()}>
      <label>
        <StyledSpan>Upload</StyledSpan>
        <input
          className="hideAccessible"
          type="file"
          accept="image/*"
          onChange={(evt) => {
            const singleFileUploaded = evt.target.files[0];
            const imgUrl = createObjectURL(singleFileUploaded);
            const imgName = singleFileUploaded.name;
            addImageLayer(imgUrl, imgName);
          }}
        />
      </label>
      <a href={imageUrl} download>
        Download Image
      </a>
    </form>
  );
}

FileInput.propTypes = {
  // dispatch: PropTypes.func,
};
