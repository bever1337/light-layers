import styled from 'styled-components';

export const ShowFilterControlForActiveLayer = styled.div`
  ${({ hide }) => hide && 'display: none;'}
`;
