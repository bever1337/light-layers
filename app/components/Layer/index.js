import React from 'react';
import { SCHEMA } from 'schema';
import { useSelect, useActionCreator } from 'containers/Provider/effects';
import Filter from 'components/Filter';
import { Button, Ol } from 'components/Html';
import { LayerFilterButton } from 'components/LayerFilterButton';

import { ShowFilterControlForActiveLayer } from './styles';

function asFilter(filter) {
  return <Filter filter={filter} key={filter.key} />;
}

function asAddLayerFilterButton(image, filterSchema) {
  return (
    <LayerFilterButton
      key={filterSchema.key}
      filterKey={filterSchema.key}
      parameters={filterSchema.parameters}
      image={image}
    />
  );
}

function Layer({ image, index }) {
  const { selectLayer } = useActionCreator();
  const onClickSelectLayer = React.useCallback(() => {
    selectLayer(image.objectUrl);
  }, [image.objectUrl, selectLayer]);
  const filters = useSelect(['present', 'filters', image.objectUrl], []);
  const currentLayer = useSelect('currentLayer');
  const layerIsSelected = currentLayer === image.objectUrl;

  return (
    <li>
      {index}
      <Button disabled={layerIsSelected} onClick={onClickSelectLayer}>
        {image.name}
        {layerIsSelected && '*'}
      </Button>
      <ShowFilterControlForActiveLayer hide={!layerIsSelected}>
        <div>{SCHEMA.map(asAddLayerFilterButton.bind(null, image))}</div>
        <Ol>{filters.map(asFilter).reverse()}</Ol>
      </ShowFilterControlForActiveLayer>
    </li>
  );
}

export default Layer;
