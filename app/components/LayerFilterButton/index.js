/**
 *
 * Layers
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { useActionCreator, useSelect } from 'containers/Provider/effects';
import { Button } from 'components/Html';

export function LayerFilterButton({ filterKey, image, parameters }) {
  const filters = useSelect(`present.filters.${image.objectUrl}`, []);
  const { addFilterLayer } = useActionCreator();
  return (
    <Button
      disabled={filters.some((filter) => filter.key === filterKey)}
      onClick={() => {
        addFilterLayer(
          image.objectUrl,
          filterKey,
          parameters.reduce((acc, value, index) => {
            const nextAcc = acc;
            nextAcc[index] = {
              key: value.key,
              unit: value.units[0],
              value: value.lacuna,
            };
            return nextAcc;
          }, [])
        );
      }}
    >
      {filterKey}
    </Button>
  );
}

LayerFilterButton.propTypes = {};
