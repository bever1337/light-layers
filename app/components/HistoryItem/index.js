import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useActionCreator } from 'containers/Provider/effects';
import { Button } from 'components/Html';

export function HistoryItem({ history, past, present, future }) {
  const { undo, redo } = useActionCreator();
  const onClickChangeHistory = useCallback(() => {
    if (past) {
      undo(history.timestamp);
    } else if (future) {
      redo(history.timestamp);
    }
  }, [past, present, future, history.timestamp]);
  return (
    <li>
      <Button onClick={onClickChangeHistory}>
        {history.summary}
        {present && '*'}
      </Button>
    </li>
  );
}

HistoryItem.propTypes = {
  history: PropTypes.object.isRequired,
  past: PropTypes.bool,
  present: PropTypes.bool,
  future: PropTypes.bool,
};
