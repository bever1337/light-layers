/**
 *
 * Button
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButton = styled.button``;

function Button({ children, className, disabled, onClick, type = 'button' }) {
  return (
    <StyledButton
      className={className}
      disabled={disabled}
      onClick={onClick}
      type={type}
    >
      {children}
    </StyledButton>
  );
}

Button.propTypes = {};

export default Button;
