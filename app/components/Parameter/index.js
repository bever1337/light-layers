import React /* , { useCallback } */ from 'react';
// import PropTypes from 'prop-types';
// import { debounce } from 'lodash';
import { useActionCreator } from 'containers/Provider/effects';
import { SCHEMA_MAP } from 'schema';
import { UndoableContext } from 'containers/Provider/context';

function useRange(min, max) {
  return typeof min === 'number' && typeof max === 'number';
}

export function Parameter({ parameter, filterName, schema }) {
  const { adjustFilterLayer } = useActionCreator();
  const { currentLayer } = React.useContext(UndoableContext);
  // const [localValue, setLocalValue] = React.useState(parameter.value);
  // const debouncedInput = React.useCallback(
  //   debounce(
  //     value => {
  //       dispatchAdjustFilterLayer(filterName, parameter.key, value);
  //     },
  //     420,
  //     {
  //       maxWait: 420,
  //     },
  //   ),
  //   [filterName],
  // );
  const onChange = React.useCallback(
    (evt) => {
      const inputValue = evt.target.value;
      const nextValue = inputValue || schema.lacuna;
      adjustFilterLayer(currentLayer, filterName, parameter.key, nextValue);
    },
    [filterName]
  );

  return (
    <label>
      {parameter.key}
      <input
        onChange={onChange}
        placeholder={schema.lacuna}
        type={useRange(schema.min, schema.max) ? 'range' : 'text'}
        value={parameter.value}
      />
    </label>
  );
}
