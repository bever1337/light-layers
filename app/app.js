import { createElement } from 'react';
import { render } from 'react-dom';
import { App } from 'containers/App';

/* eslint-disable func-names */
(function(MOUNT_NODE) {
  render(createElement(App), MOUNT_NODE);
})(document.getElementById('app'));
