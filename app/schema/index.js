export const SCHEMA = [
  {
    key: 'blur',
    parameters: [
      {
        key: 'radius',
        lacuna: 0,
        max: false,
        min: 0,
        units: ['px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt'],
      },
    ],
    transformer: (radius = 0, unit = 'px') => `blur(${radius}${unit})`,
  },
  {
    key: 'brightness',
    parameters: [
      {
        key: '%',
        lacuna: 100,
        max: false,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 100, unit = '%') =>
      `brightness(${percentage}${unit})`,
  },
  {
    key: 'contrast',
    parameters: [
      {
        key: '%',
        lacuna: 100,
        max: false,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 100, unit = '%') =>
      `contrast(${percentage}${unit})`,
  },
  {
    key: 'dropShadow',
    parameters: [
      {
        key: 'offset x',
        lacuna: 0,
        max: false,
        min: false,
        units: ['px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt'],
      },
      {
        key: 'offset y',
        lacuna: 0,
        max: false,
        min: false,
        units: ['px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt'],
      },
      {
        key: 'blur radius',
        lacuna: 0,
        max: false,
        min: 0,
        units: ['px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt'],
      },
      // {
      //   key: 'spread radius',
      //   lacuna: 0,
      //   max: false,
      //   min: 0,
      //   units: ['px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt'],
      // },
      {
        key: 'color',
        lacuna: 0xffffff,
        max: 0xffffff,
        min: 0x000000,
        units: [],
      },
    ],
    transformer: (
      offsetX = 0,
      offsetXUnit = 'px',
      offsetY = 0,
      offsetYUnit = 'px',
      blurRadius = 0,
      blurRadiusUnit = 'px',
      // spreadRadius = 0,
      // spreadRadiusUnit = 'px', // fahkin' unsupported? maybe browser compatibility testing required
      color = 0x000000
    ) =>
      `drop-shadow(${offsetX}${offsetXUnit} ${offsetY}${offsetYUnit} ${blurRadius}${blurRadiusUnit} #${color ||
        parseInt(color, 16)})`,
  },
  {
    key: 'grayscale',
    parameters: [
      {
        key: '%',
        lacuna: 0,
        max: 100,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 0, unit = '%') =>
      `grayscale(${percentage}${unit})`,
  },
  {
    key: 'hueRotate',
    parameters: [
      {
        key: 'angle',
        lacuna: 0,
        max: false,
        min: false,
        units: ['deg', 'grad', 'rad', 'turn'],
      },
    ],
    transformer: (angle = 0, unit = 'deg') => `hue-rotate(${angle}${unit})`,
  },
  {
    key: 'invert',
    parameters: [
      {
        key: '%',
        lacuna: 0,
        max: 100,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 0, unit = '%') => `invert(${percentage}${unit})`,
  },
  {
    key: 'opacity',
    parameters: [
      {
        key: '%',
        lacuna: 100,
        max: 100,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 100, unit = '%') =>
      `opacity(${percentage}${unit})`,
  },
  {
    key: 'saturate',
    parameters: [
      {
        key: '%',
        lacuna: 100,
        max: false,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 100, unit = '%') =>
      `saturate(${percentage}${unit})`,
  },
  {
    key: 'sepia',
    parameters: [
      {
        key: '%',
        lacuna: 0,
        max: 100,
        min: 0,
        units: ['%'],
      },
    ],
    transformer: (percentage = 0, unit = '%') => `sepia(${percentage}${unit})`,
  },
];

export const SCHEMA_MAP = SCHEMA.reduce((acc, value) => {
  const nextAcc = acc;
  acc[value.key] = value;
  return nextAcc;
}, {});

export default SCHEMA;
