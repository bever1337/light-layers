import React, { useCallback, useEffect } from 'react';

import { ImageReference } from 'components/ImageReference';
import { useActionCreator } from 'containers/Provider/effects';
import { asObjectUrl, redrawEachCanvas } from './utils';

export function useRedrawSubCanvases({ canvasRef, images }) {
  const { setImageUrl } = useActionCreator();
  return useCallback(() => {
    const { current: parentCanvas } = canvasRef;
    const context = parentCanvas.getContext('2d');
    context.clearRect(0, 0, parentCanvas.width, parentCanvas.height);
    const subCanvases = images.map(asObjectUrl);
    subCanvases.forEach(redrawEachCanvas.bind(null, context));
    const imageUrl = parentCanvas.toDataURL();
    setImageUrl(imageUrl);
  }, [canvasRef, images]);
}

export function useImageReferenceMapCallback({ canvasRef, draw, images }) {
  return useCallback(
    (imageLayer) => (
      <ImageReference
        draw={draw}
        ref={canvasRef}
        key={imageLayer.objectUrl}
        imageLayer={imageLayer}
      />
    ),
    [draw, images, canvasRef]
  );
}

export function useRedrawCanvasWhenImagesAddedOrRemoved({
  draw,
  images,
  imageUrl,
}) {
  useEffect(() => draw(), [draw, images.length, imageUrl]);
}
