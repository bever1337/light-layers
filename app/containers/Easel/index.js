/**
 *
 * Canvas
 *
 */

import React, { useEffect, useContext } from 'react';
// import PropTypes from 'prop-types';

import { useSelect } from 'containers/Provider/effects';
import { UndoableContext } from 'containers/Provider/context';
import { Canvas } from 'components/Html';
import {
  useRedrawSubCanvases,
  useImageReferenceMapCallback,
  useRedrawCanvasWhenImagesAddedOrRemoved,
} from './hooks';
import { PrimaryImage } from './styles';

export function Easel() {
  const images = useSelect('present.images', []);
  const imageUrl = useSelect('imageUrl', '/');
  const imageRef = React.useRef();
  const canvasRef = React.useRef();
  const draw = useRedrawSubCanvases({ canvasRef, images });
  const asImageReference = useImageReferenceMapCallback({
    canvasRef,
    draw,
    images,
  });
  useRedrawCanvasWhenImagesAddedOrRemoved({ draw, images, imageUrl });
  // useEffect(() => {}, [canvasRef]);
  return (
    <React.Fragment>
      <PrimaryImage ref={imageRef} alt="" src={imageUrl} />
      <Canvas ref={canvasRef}>{images.map(asImageReference)}</Canvas>
    </React.Fragment>
  );
}

Easel.propTypes = {};
