import styled from 'styled-components';

export const PrimaryImage = styled.img`
  height: 100%;
  width: 100%;
  object-fit: contain;
`;
