export function asObjectUrl(image) {
  return document.getElementById(image.objectUrl);
}

export function redrawEachCanvas(context, canvas) {
  context.drawImage(canvas, 0, 0, canvas.width, canvas.height);
}
