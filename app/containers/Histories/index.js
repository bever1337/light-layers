import React, { useCallback, useContext } from 'react';
import { Button } from 'components/Html';
import { HistoryItem } from 'components/HistoryItem';
import { UndoableContext } from 'containers/Provider/context';
import { useActionCreator } from 'containers/Provider/effects';

function asPast(props = {}) {
  return <HistoryItem key={props.timestamp} history={props} past />;
}

function asPresent(props = {}) {
  return <HistoryItem key={props.timestamp} history={props} present />;
}

function asFuture(props = {}) {
  return <HistoryItem key={props.timestamp} history={props} future />;
}

export function Histories() {
  const { past, present, future } = useContext(UndoableContext);
  const { redo, undo } = useActionCreator();
  const undoHistoryOnClick = useCallback(() => undo(), [undo]);
  const redoHistoryOnClick = useCallback(() => redo(), [redo]);
  return (
    <div>
      <Button onClick={undoHistoryOnClick} disabled={past.length === 0}>
        Undo
      </Button>
      <Button onClick={redoHistoryOnClick} disabled={future.length === 0}>
        Redo
      </Button>
      <ol>
        {past.map(asPast)}
        {asPresent(present)}
        {future.map(asFuture)}
      </ol>
    </div>
  );
}

History.propTypes = {};
