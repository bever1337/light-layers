/**
 *
 * Layers
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';

import { Ol } from 'components/Html';
import { useSelect } from 'containers/Provider/effects';
import Layer from 'components/Layer';

function asLayer(image, index) {
  return <Layer image={image} index={index} key={image.objectUrl} />;
}

export function Layers() {
  const images = useSelect('present.images', []);
  return (
    <form onSubmit={(evt) => evt.preventDefault()}>
      <Ol>{images.map(asLayer).reverse()}</Ol>
    </form>
  );
}

Layers.propTypes = {};
