import React, { useRef, useReducer } from 'react';
import { UndoableContext } from './context';
import { useLogging, useUndoReducer, useCombineReducers } from './effects';
import { initialState, reducer, uiReducer, initialUiState } from './reducer';

export function UndoProvider({ children }) {
  // const {
  //   actions,
  //   dispatch,
  //   state: { past, present: state, future },
  // } = useUndoReducer(reducer, initialState);
  const banana = useUndoReducer(reducer, initialState);
  const foo = useReducer(uiReducer, initialUiState);
  const { state: combinedState, dispatch, actions } = useCombineReducers(
    foo,
    banana
  );
  const { past, present: state, future } = combinedState;
  useLogging(combinedState);
  return (
    <UndoableContext.Provider
      value={{
        actions,
        dispatch,
        ...combinedState,
      }}
    >
      {children}
    </UndoableContext.Provider>
  );
}
