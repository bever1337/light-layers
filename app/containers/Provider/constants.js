// history action types
export const NEW_LAYER = '@@LL/NEW_LAYER';
export const REDO = 'REDO';
export const UNDO = 'UNDO';

// present action types
export const ADD_IMAGE_LAYER = 'app/Form/ADD_IMAGE_LAYER';
export const ADD_FILTER_LAYER = 'app/Form/ADD_FILTER_LAYER';
export const ADJUST_FILTER_LAYER = 'app/Form/ADJUST_FILTER_LAYER';
export const SELECT_LAYER = 'app/Form/SELECT_LAYER';
export const SET_IMAGE_URL = 'app/Form/SET_IMAGE_URL';
