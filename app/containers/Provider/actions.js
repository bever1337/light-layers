import {
  REDO,
  UNDO,
  ADD_FILTER_LAYER,
  ADD_IMAGE_LAYER,
  ADJUST_FILTER_LAYER,
  SELECT_LAYER,
  SET_IMAGE_URL,
} from './constants';

// internal action creators
export function redo(timestamp) {
  return {
    type: REDO,
    timestamp,
  };
}

export function undo(timestamp) {
  return {
    type: UNDO,
    timestamp,
  };
}

// present state action creators
export function addImageLayer(objectUrl, name) {
  return {
    type: ADD_IMAGE_LAYER,
    name,
    objectUrl,
    summary: 'New image',
  };
}

export function addFilterLayer(imageName, filterName, values) {
  return {
    type: ADD_FILTER_LAYER,
    imageName,
    name: filterName,
    summary: `Add ${filterName} filter`,
    values,
  };
}

export function adjustFilterLayer(
  layer,
  filterName,
  parameterKey,
  value,
  unit
) {
  return {
    type: ADJUST_FILTER_LAYER,
    filterName,
    parameterKey,
    layer,
    summary: `Edit ${filterName}`,
    unit,
    value,
  };
}

export function selectLayer(layerName) {
  return {
    type: SELECT_LAYER,
    layerName,
    summary: 'Select layer',
  };
}

export function setImageUrl(imageUrl) {
  return {
    type: SET_IMAGE_URL,
    imageUrl,
  };
}
