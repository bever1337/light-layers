import * as actionCreators from './actions';

function valueHasKey(previousValue, nextKey) {
  return (
    previousValue !== null &&
    previousValue !== undefined &&
    Object.prototype.hasOwnProperty.call(previousValue, nextKey)
  );
}

export function reduceValueFromKeypath(previousValue, nextKey) {
  if (valueHasKey(previousValue, nextKey)) return previousValue[nextKey];
  return undefined;
}

export function parameterReductionWithImageData(imageName, objectUrl) {
  return function reductionCallback(acc, value) {
    acc.push(Object.assign({}, value, { imageName, objectUrl }));
    return acc;
  };
}

export function findPatternAtKey(pattern, key) {
  return function findCallback(match) {
    return typeof key === 'number' || typeof key === 'string'
      ? match[key] === pattern
      : match === pattern;
  };
}

export function mapActionCreators(dispatch) {
  function reductionByActionCreatorKey(acc, [key, actionCreator]) {
    acc[key] = (...parameters) => dispatch(actionCreator(...parameters));
    return acc;
  }
  function reduceActionCreators() {
    return Object.entries(actionCreators).reduce(
      reductionByActionCreatorKey,
      {}
    );
  }
  return reduceActionCreators;
}
