const combineReducers = (reducerA, reducerB) => {
  const state = {
    ...reducerA[0],
    ...reducerB[0],
  };
  function dispatch(action) {
    reducerA[1](action);
    reducerB[1](action);
  }
  return [state, dispatch];
};

export default combineReducers;
