/* eslint-disable func-names */
import { useCallback, useContext, useEffect, useMemo, useReducer } from 'react';
import { UndoableContext } from './context';
import { REDO, UNDO } from './constants';
import { mapActionCreators, reduceValueFromKeypath } from './utils';
import combineReducers from './combineReducers';

export function useActionCreator() {
  const { actions } = useContext(UndoableContext);
  return actions;
}

export function useSelect(delimitedKeypath, defaultValue) {
  const state = useContext(UndoableContext);
  const memoizedSelectedValue = useMemo(() => {
    const selectedValue = (Array.isArray(delimitedKeypath)
      ? delimitedKeypath
      : delimitedKeypath.split('.')
    ).reduce(reduceValueFromKeypath, state);
    return selectedValue === undefined ? defaultValue : selectedValue;
  }, [delimitedKeypath, defaultValue, state]);
  return memoizedSelectedValue;
}

const initialUndoableState = {
  past: [],
  present: undefined,
  future: [],
};

function filterPastHistoricalStates(timestamp) {
  return function filterCallback(historicalState) {
    return historicalState.timestamp < timestamp;
  };
}

function findPresentHistoricalState(timestamp) {
  return function findCallback(historicalState) {
    return historicalState.timestamp === timestamp;
  };
}

function findFutureHistoricalState(timestamp) {
  return function filterCallback(historicalState) {
    return historicalState.timestamp > timestamp;
  };
}

function setNewPresent(timestamp, historicalStates) {
  const nextPast = historicalStates.filter(
    filterPastHistoricalStates(timestamp)
  );
  const nextPresent = historicalStates.find(
    findPresentHistoricalState(timestamp)
  );
  const nextFuture = historicalStates.filter(
    findFutureHistoricalState(timestamp)
  );
  return {
    past: nextPast,
    present: nextPresent,
    future: nextFuture,
  };
}

function shiftForwards(state) {
  return {
    past: state.past.concat(state.present),
    present: state.future[0],
    future: state.future.slice(1),
  };
}

function shiftBackwards(state) {
  return {
    past: state.past.slice(0, state.past.length - 1),
    present: state.past[state.past.length - 1],
    future: [state.present].concat(state.future),
  };
}

export function useUndoReducer(reducer, initialStateProps) {
  const initializeUndoableReducer = useCallback(
    (initialState) =>
      Object.assign({}, initialState, {
        present: Object.assign({}, reducer(initialStateProps, {}), {
          timestamp: Date.now(),
        }),
      }),
    [reducer, initialStateProps]
  );
  const undoableReducer = useCallback(
    (state, action) => {
      switch (action.type) {
        case UNDO:
        case REDO: {
          const { timestamp } = action;
          if (timestamp) {
            const historicalStates = [
              ...state.past,
              state.present,
              ...state.future,
            ];
            return setNewPresent(timestamp, historicalStates);
          }
          return action.type === UNDO
            ? shiftBackwards(state)
            : shiftForwards(state);
        }
        default: {
          const nextPresent = reducer(state.present, action);
          if (nextPresent === state.present) {
            return state;
          }
          return {
            past: state.past.concat(state.present),
            present: Object.assign({}, nextPresent, { timestamp: Date.now() }),
            future: [],
          };
        }
      }
    },
    [reducer]
  );
  const initializedUndoReducer = useReducer(
    undoableReducer,
    initialUndoableState,
    initializeUndoableReducer
  );

  // const actions = useMemo(mapActionCreators(dispatch), [dispatch]);
  // return { actions, dispatch, state };
  return initializedUndoReducer;
}

export function useLogging(state) {
  useEffect(() => {
    console.log('state changed: ', state);
  }, [state]);
}

export function useCombineReducers(reducerA, reducerB) {
  const [state, dispatch] = useMemo(() => combineReducers(reducerA, reducerB), [
    reducerA,
    reducerB,
  ]);
  const actions = useMemo(mapActionCreators(dispatch), [dispatch]);
  return { state, dispatch, actions };
}
