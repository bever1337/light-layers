import { createContext } from 'react';

export const UndoableContext = createContext();
