import {
  ADD_FILTER_LAYER,
  ADD_IMAGE_LAYER,
  ADJUST_FILTER_LAYER,
  NEW_LAYER,
  SELECT_LAYER,
  SET_IMAGE_URL,
} from './constants';
import { findPatternAtKey, parameterReductionWithImageData } from './utils';

export const initialState = {
  filters: {},
  images: [],
  summary: 'New project',
};

export function reducer(state, action) {
  switch (action.type) {
    case ADD_IMAGE_LAYER: {
      const newFilters = {};
      newFilters[action.objectUrl] = [];
      const newImage = {
        name: action.name,
        objectUrl: action.objectUrl,
      };
      return {
        filters: Object.assign({}, state.filters, newFilters),
        images: state.images.concat(newImage),
        summary: action.summary,
      };
    }
    case ADD_FILTER_LAYER: {
      const objectUrl = action.imageName;
      const imageName = state.images.find(
        findPatternAtKey(objectUrl, 'objectUrl')
      ).name;
      const newLayer = {};
      newLayer[objectUrl] = state.filters[objectUrl].concat({
        key: action.name,
        objectUrl,
        parameters: action.values.reduce(
          parameterReductionWithImageData(imageName, objectUrl),
          []
        ),
        imageName,
      });
      return Object.assign({}, state, {
        filters: Object.assign({}, state.filters, newLayer),
        summary: action.summary,
      });
    }
    case ADJUST_FILTER_LAYER: {
      const { layer, filterName, parameterKey, value, unit } = action;
      const newLayer = state.filters[layer].map((filter) => {
        if (filter.key === filterName) {
          const paramIndex = filter.parameters.findIndex(
            findPatternAtKey(parameterKey, 'key')
          );
          return Object.assign({}, filter, {
            parameters: [
              ...filter.parameters.slice(0, paramIndex),
              {
                ...filter.parameters[paramIndex],
                value,
                unit,
              },
              ...filter.parameters.slice(paramIndex + 1),
            ],
          });
        }
        return filter;
      });
      const newLayerObject = {};
      newLayerObject[layer] = newLayer;
      return Object.assign({}, state, {
        filters: Object.assign({}, state.filters, newLayerObject),
        summary: action.summary,
      });
    }
    default:
      return state;
  }
}

export const initialUiState = {
  imageUrl: null,
  currentLayer: NEW_LAYER,
};

export function uiReducer(state, action) {
  switch (action.type) {
    case SELECT_LAYER:
      return Object.assign({}, state, {
        currentLayer: action.layerName,
      });
    case ADD_IMAGE_LAYER:
      return Object.assign({}, state, {
        currentLayer: action.objectUrl,
      });
    case SET_IMAGE_URL:
      return Object.assign({}, state, {
        imageUrl: action.imageUrl,
      });
    default:
      return state;
  }
}
