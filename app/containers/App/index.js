import React from 'react';

import { Easel } from 'containers/Easel';
import { FileInput } from 'components/FileInput';
import { Histories } from 'containers/Histories';
import { Layers } from 'containers/Layers';
import { UndoProvider } from 'containers/Provider';

import { GlobalStyle, Main } from './styles';

export function App() {
  return (
    <React.StrictMode>
      <GlobalStyle />
      <UndoProvider>
        <header />
        <Main>
          <FileInput />
          <Layers />
          <Easel />
          <Histories />
        </Main>
        <footer />
      </UndoProvider>
    </React.StrictMode>
  );
}
